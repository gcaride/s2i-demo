# Nginx HTTP server and reverse proxy (nginx) S2I Sample Application

The application serves a single static html page via nginx.

To build and run the application:

```
$ s2i build https://gitlab.com/gcaride/s2i-demo centos/nginx-116-centos7 nginxs2i
$ docker run -p 8080:8080 nginxs2i
$ # browse to http://localhost:8080
```